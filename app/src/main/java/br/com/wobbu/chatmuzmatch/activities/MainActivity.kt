package br.com.wobbu.chatmuzmatch.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.wobbu.chatmuzmatch.R
import br.com.wobbu.chatmuzmatch.dao.MessageDao
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val VIEW_TYPE_SENDER = 1
    private val VIEW_TYPE_RECEIVER = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initUI()
    }

    private fun initUI() {
        bt_sender.setOnClickListener(senderClick)
        bt_receiver.setOnClickListener(receiverClick)
        bt_clean.setOnClickListener(cleanChat)
    }

    private val senderClick = View.OnClickListener {
        val intent = Intent(this, MessageActivity::class.java)
        intent.putExtra("userId", VIEW_TYPE_SENDER)
        startActivity(intent)
    }
    private val receiverClick = View.OnClickListener {
        val intent = Intent(this, MessageActivity::class.java)
        intent.putExtra("userId", VIEW_TYPE_RECEIVER)
        startActivity(intent)
    }

    private val cleanChat = View.OnClickListener {
        val dao = MessageDao()
        dao.cleanAll()
        Toast.makeText(this, "Clean all messages.", Toast.LENGTH_SHORT).show()
    }
}

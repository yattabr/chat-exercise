package br.com.wobbu.chatmuzmatch

import android.app.Application
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import io.realm.Realm
import io.realm.Realm.setDefaultConfiguration
import io.realm.RealmConfiguration


class ChatApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
            .name(BuildConfig.DB_NAME)
            .deleteRealmIfMigrationNeeded()
            .build()
        setDefaultConfiguration(realmConfiguration)
    }
}
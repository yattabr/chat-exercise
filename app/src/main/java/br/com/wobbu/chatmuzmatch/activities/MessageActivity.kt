package br.com.wobbu.chatmuzmatch.activities

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.wobbu.chatmuzmatch.R
import br.com.wobbu.chatmuzmatch.adapter.MessageListAdapter
import br.com.wobbu.chatmuzmatch.model.Message
import br.com.wobbu.chatmuzmatch.viewModel.MessageViewModel
import kotlinx.android.synthetic.main.activity_message.*


class MessageActivity : AppCompatActivity() {

    private lateinit var messageViewModel: MessageViewModel
    private var userId: Int = 0
    private lateinit var adapter: MessageListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)

        messageViewModel = ViewModelProviders.of(this).get(MessageViewModel::class.java)

        initUI()
        initObservers()

    }

    private fun initUI() {
        userId = intent.getIntExtra("userId", 0)

        bt_send.setOnClickListener(sendClick)

        mountRecyclerView(messageViewModel.getAllMessage())

        // Scroll the list to the last position
        recycler.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            recycler.scrollToPosition(recycler.adapter!!.itemCount - 1)
        }
    }

    private fun initObservers() {
        messageViewModel.messageObserver.observe(this, Observer {
            when (it) {
                is Message -> println(it.message)
            }
        })

        messageViewModel.getAllMessageObserver().observe(this, Observer {
            mountRecyclerView(messageViewModel.convertRealmToArrayList(it))
        })
    }

    private fun mountRecyclerView(results: ArrayList<Message>) {
        adapter = MessageListAdapter(results)
        recycler.adapter = adapter
        recycler.scrollToPosition(recycler.adapter!!.itemCount - 1)
        edit_message.text.clear()

    }

    private val sendClick = View.OnClickListener {
        if (edit_message.text.toString() != "") {
            messageViewModel.sendMessage(userId, edit_message.text.toString())
            edit_message.hideKeyboard()
        }
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}
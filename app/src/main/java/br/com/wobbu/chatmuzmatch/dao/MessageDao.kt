package br.com.wobbu.chatmuzmatch.dao

import androidx.lifecycle.LiveData
import br.com.wobbu.chatmuzmatch.model.Message
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmResults

class MessageDao {

    private val realm by lazy {
        Realm.getDefaultInstance()
    }

    fun insertMessage(message: Message) {
        realm.executeTransaction {
            it.insertOrUpdate(message)
        }
    }

    fun findMessageByUserId(userId: Int): LiveData<RealmResults<Message>> {
        return realm.where(Message::class.java).equalTo("userId", userId).findAll().asLiveData()
    }

    fun getAllMessages(): RealmResults<Message> {
        return realm.where(Message::class.java).findAll()
    }

    fun getAllMessagesObserver(): LiveData<RealmResults<Message>> {
        return realm.where(Message::class.java).findAll().asLiveData()
    }

    fun cleanAll() {
        realm.executeTransaction {
            it.deleteAll()
        }
    }

    private fun <T : RealmModel> RealmResults<T>.asLiveData() = RealmLiveData(this)
}
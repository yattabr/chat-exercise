package br.com.wobbu.chatmuzmatch.model

import io.realm.RealmObject

open class Message : RealmObject() {
    var userId: Int = 0
    var message: String = ""
    var startDateTime: String = ""
    var endDateTime: String = ""
    var isTail: Boolean = false
}
package br.com.wobbu.chatmuzmatch.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.wobbu.chatmuzmatch.R
import br.com.wobbu.chatmuzmatch.model.Message
import kotlinx.android.synthetic.main.item_receiver_message.view.*
import kotlinx.android.synthetic.main.item_sent_message.view.*

class MessageListAdapter(private val mMessageList: List<Message>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_SENDER = 1
    private val VIEW_TYPE_RECEIVER = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return if (viewType == VIEW_TYPE_SENDER) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.item_sent_message, parent, false)
            SentMessageHolder(view)
        } else {
            view = LayoutInflater.from(parent.context).inflate(R.layout.item_receiver_message, parent, false)
            ReceivedMessageHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return mMessageList.size
    }

    override fun getItemViewType(position: Int): Int {
        val message = mMessageList[position]
        return if (message.userId == VIEW_TYPE_SENDER) {
            VIEW_TYPE_SENDER
        } else {
            VIEW_TYPE_RECEIVER
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_SENDER -> {
                holder as SentMessageHolder
                holder.bind(mMessageList[position])
            }
            VIEW_TYPE_RECEIVER -> {
                holder as ReceivedMessageHolder
                holder.bind(mMessageList[position])
            }
        }
    }

    class SentMessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(message: Message) {
            itemView.text_sent_message.text = message.message
        }
    }

    class ReceivedMessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(message: Message) {
            itemView.text_received_message.text = message.message
        }
    }
}
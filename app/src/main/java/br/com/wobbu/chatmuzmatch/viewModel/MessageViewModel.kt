package br.com.wobbu.chatmuzmatch.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.wobbu.chatmuzmatch.dao.MessageDao
import br.com.wobbu.chatmuzmatch.model.Message
import io.realm.RealmResults
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MessageViewModel : ViewModel() {

    val messageObserver = MutableLiveData<Any>()
    private val messageDao = MessageDao()

    fun sendMessage(userId: Int, messageText: String) {
        val message = Message()
        message.userId = userId
        message.message = messageText

        val format = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.UK)
        val hour = format.format(Date())
        message.startDateTime = hour
        messageDao.insertMessage(message)
    }

    fun getMessageByUserId(userId: Int): LiveData<RealmResults<Message>> {
        return messageDao.findMessageByUserId(userId)
    }

    fun getAllMessage(): ArrayList<Message> {
        return convertRealmToArrayList(messageDao.getAllMessages())
    }

    fun getAllMessageObserver(): LiveData<RealmResults<Message>> {
        return messageDao.getAllMessagesObserver()
    }

    fun convertRealmToArrayList(results: RealmResults<Message>): ArrayList<Message> {
        val list = java.util.ArrayList<Message>()
        list.addAll(results)
        return list
    }


}